export const Utils = {
    methods  : {
        /** Méthode setToken
         * Ajoute le token en paramètre dans la requete axios
         * @param {*} token 
         */
        setToken(token){
            window.axios.defaults.params.token = token;
        },

        /** Méthode memberConnected
         * Vérifie si un membre est bien connecté,
         * Si Oui appel setToken et @return true
         * Sinon @return false
         */
        memberConnected(){
            if(this.$store.state.member === false){
                return false;
            }
            else{
                this.setToken(this.$store.state.token);
                return true;
            }
        }
    }
}