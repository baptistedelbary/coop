import Vue from 'vue'
import Router from 'vue-router'
import signup from '@/components/signup'
import signin from '@/components/signin'
import dashboard from '@/components/dashboard'
import listMembers from '@/components/listMembers'
import channel from '@/components/channel'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'signin',
      component: signin
    },
    {
      path: '/signup',
      name: 'signup',
      component: signup
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: dashboard
    },
    {
      path: '/members',
      name: 'listMembers',
      component: listMembers
    },
    {
      path: '/channel/:id',
      name: 'channel',
      component: channel
    },
  ]
})
