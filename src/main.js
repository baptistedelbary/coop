// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import store from './store'
import Gravatar from 'vue-gravatar';
import VueChatScroll from 'vue-chat-scroll'
import moment from 'moment'
import {Utils} from './components/mixin/utils.js'
Vue.use(VueChatScroll);
Vue.mixin(Utils);

moment.locale('fr');

Vue.prototype.moment = moment;

Vue.config.productionTip = false

Vue.component('v-gravatar', Gravatar);

//INIT store
store.subscribe((mutation, state) => {
  localStorage.setItem('store', JSON.stringify(state));
});

// API KEY : ca517768bddd4711b56733c057705959
window.axios = axios.create(
  { 
    baseURL: 'http://coop.api.netlor.fr/api',
    params : { token : false },
    headers: { Authorization: 'Token token=ca517768bddd4711b56733c057705959' } 
  });

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  beforeCreate() {
    this.$store.commit('initialiseStore');
  },
})
